<?php
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use SoatdevTranslations\Text;

class Test extends TestCase
{
	use DatabaseTransactions;


	protected $application;
	protected $website;
	protected $texts = [];
	public function setUp()
	{
		parent::setUp();
		$this->application = factory(\Soatdevly\Application::class)->create([
			'domain'=>'foobar.local', 
			'subdomain'=>'foobar'
		]);
		$this->website = factory(\Soatdevly\Website::class)->create([
			'application_id'=>$this->application->id, 
			'domain'=>'foo.local',
			'subdomain'=>'foo',
			'template'=>'foobar',
			'color'=>'blue',
		]);

		$this->texts = factory(Text::class, 3)->create([
				'language_code'=>'fr',
				'owner_class' => 'Soatdevly\Website',
				'owner_id_attribute' => 'id',
				'owner_id_value' => $this->website->id,
			]);
	}

	/** @test */
	function it_knows_to_which_entity_it_belongs()
	{
		$this->assertEquals($this->texts[0]->parentEntity()->id, $this->website->id);
	}

	/** @test */
	function it_can_find_all_texts_for_an_entity()
	{

		$retrievedTexts = Text::allForEntity($this->website);

		$this->assertContains($this->texts[0]->id, $retrievedTexts->lists('id'));
		
	}

	/** @test */
	function it_can_find_a_text_for_an_entity_from_code()
	{
		$websiteTitle = factory(Text::class)->create([
				'owner_class' => 'Soatdevly\Website',
				'owner_id_attribute' => 'id',
				'owner_id_value' => $this->website->id,
				'code' => 'website-title',
				'value' => 'My First Website'
			]);
		$retrievedText = Text::fromCodeForEntity('website-title', $this->website);
		$this->assertEquals("My First Website", $retrievedText->value);
	}
	/** @test */
	function it_can_add_a_text_to_an_entity()
	{
		Text::add($this->website, 'website-title', 'My First Website');
		$retrievedText = Text::fromCodeForEntity('website-title', $this->website);

		$this->assertEquals("My First Website", $retrievedText->value);
	}
	/** @test */
	function it_can_add_and_retrieve_a_translation()
	{
		Text::add($this->website, 'website-title', 'My First Website');
		
		$title = Text::fromCodeForEntity('website-title', $this->website);
		$title->translate('Mon  premier site', 'fr');

		$this->assertEquals("Mon  premier site", $title->value('fr'));

	}
	/** @test */
	function it_updates_the_value_if_adding_a_translation_that_already_exists_for_a_text()
	{
		Text::add($this->website, 'website-title', 'My First Website');
		$title = Text::fromCodeForEntity('website-title', $this->website);
		
		$title->translate('Mon  premier site', 'en');
		$this->assertEquals('Mon  premier site', $title->value('en'));
	}

	/** @test */
	function it_throws_an_exception_when_adding_a_text_without_entity_default_language()
	{
		$this->setExpectedException('Exception');
		$text = Text::add($this->website, 'website-title', 'Test exception', 'es');
	}

	/** @test */
	function it_can_add_a_text_with_multiple_translations_at_once()
	{
		$t = array(
			'en'=>'My English title',
			'fr'=>'Mon titre francais',
			'es'=>'Mi titulo en espanol',
		);

		Text::add($this->website, 'website-title', $t);

		$title = Text::fromCodeForEntity('website-title', $this->website);
		$this->assertEquals($title->value('en'), 'My English title');
		$this->assertEquals($title->value('fr'), 'Mon titre francais');
		$this->assertEquals($title->value('es'), 'Mi titulo en espanol');
		$this->assertCount(2, $title->translations);
		
	}

	/** @test */
	function it_returns_the_default_value_when_the_language_does_not_exist()
	{
		Text::add($this->website, 'website-title', 'My First Website');
		
		$title = Text::fromCodeForEntity('website-title', $this->website);
		
		$this->assertEquals("My First Website", $title->value('00'));
	}

	/** @test */
	function it_can_update_its_value_or_its_translations_values()
	{
		$this->texts[0]->translate('My new translate', 'fr');
		$this->assertEquals("My new translate", $this->texts[0]->value('fr'));
		$this->texts[0]->translate('What the f***', 'en');
		$this->assertEquals("What the f***", $this->texts[0]->value('en'));
		$this->texts[0]->translate('What the f***', 'fr');
		$this->assertEquals("What the f***", $this->texts[0]->value('fr'));
	}

	/** @test */
	function it_can_store_itself_in_the_db_from_input_values()
	{
		$data = ['footernote' => array(
			'en'=>'My english title',
			'fr'=>'Mon titre francais',
			'es'=>'Mi titulo en espanol'),
		];
		
		\SoatdevTranslations\Text::add($this->website, 'footernote', $data['footernote']);

		//fromCodeForEntity($code, $entity, $identifying_attribrute = 'id')
		$t = \SoatdevTranslations\Text::fromCodeForEntity('footernote', $this->website);
		$this->assertEquals('My english title', $t->value('en'));
		$this->assertEquals('Mon titre francais', $t->value('fr'));
		$this->assertEquals('Mi titulo en espanol', $t->value('es'));

	}

	/** @test */
	function it_knows_if_it_has_to_be_represented_by_a_textarea()
	{
		$this->website->textareas = ['description', 'longtext', 'anothertest'];
		
		//$this->texts[0]->isTextarea()
	}


}