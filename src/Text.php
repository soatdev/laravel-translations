<?php

namespace SoatdevTranslations;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
	protected $fillable = ['owner_class', 'owner_id_attribute', 'owner_id_value', 'language_code', 'code', 'value'];
	public static function allForEntity($entity, $identifying_attribrute = 'id')
	{
		return Text::where('owner_class', '=', get_class ($entity))
			->where('owner_id_attribute', '=', $identifying_attribrute)
			->where('owner_id_value', '=', $entity->$identifying_attribrute)
			->get();
	}

	public static function fromCodeForEntity($code, $entity, $identifying_attribrute = 'id')
	{
		return Text::where('owner_class', '=', get_class ($entity))
			->where('owner_id_attribute', '=', $identifying_attribrute)
			->where('owner_id_value', '=', $entity->$identifying_attribrute)
			->where('code', '=', $code)
			->first();
	}


	public static function add($entity, $code, $value, $language_code=null)
	{
		
		if(is_array($value))
		{
			//Create the text for the default language
			if(isset($entity->language_code)){
				if(!array_key_exists($entity->language_code, $value)){
					throw new \Exception("The default language value for this entity was not provided.", 1);
				}
				$language_code = $entity->language_code;
			}
			elseif(!is_null($language_code)){ 
				if(!array_key_exists($language_code, $value)){
					throw new \Exception("The value for the language ".$language_code." this entity was not provided.", 1);
				}
			}
			$text = static::addText($entity, $code, $value[$language_code], $language_code);

			//Then add its translations
			unset($value[$language_code]);
			foreach ($value as $language=>$v) {
				$text->translate($v, $language);
			}
			return $text;
		}
		return static::addText($entity, $code, $value, $language_code);
	}

	protected static function addText($entity, $code, $value, $language_code=null)
	{
		//First create the text object for the default language
		$text = static::fromCodeForEntity($code, $entity);
		//If it exists, update it
		if($text){
			if($text->language_code == $language_code){
				$text->value = $value;
				$text->save();
			}
			else{
				$text->translate($value, $language_code);
			}
		}			
		else{
			//If the entity has a language code and it is different from the text added, trow an exception
			if(isset($entity->language_code) && $language_code && $language_code != $entity->language_code )
				throw new \Exception("This text is not in the default language of the entity. Please first create the text for the default language of this entity", 1);

			//Otherwise create it
			$text = new Text([
				'owner_class'=>get_class ($entity),
				'owner_id_attribute'=>'id',
				'owner_id_value'=>$entity->id,
				'language_code'=> $language_code?:\App::getLocale(),
				'code'=> $code,
				'value'=>$value,
			]);
			$text->save();
		}
		return $text;
	}

	public function parentEntity()
	{
		$staticClass = new $this->owner_class();

		return $staticClass::where($this->owner_id_attribute, '=', $this->owner_id_value)->first();
		
	}



	public function translate($value, $language_code=null)
	{
		if(is_null($language_code))
			$language_code = \App::getLocale();

		//Check if current text a same language
		if($language_code == $this->language_code)
		{
			//update current text
			$this->value = $value;
			$this->save();
			return true;
		}

		//Check if the translation exists
		$translation = $this->translations->where('language_code', $language_code)->first();

		if($translation){
			//Update it if it exists
			$translation->value = $value;
			return $translation->save();
		}
		//Otherwise create it
		else{

			$translation = new TextTranslation([
				'text_id'=>$this->id,
				'language_code'=> $language_code,
				'value'=>$value,
			]);
			$translation->save();
		}
	}

	public function translations(){
		return $this->hasMany('TextTranslation');
	}

	public function value($language_code = null)
	{
		if(is_null($language_code))
			return $this->__toString();

		if($this->language_code == $language_code)
			return $this->value;

		
		$translation = $this->translations()->where('language_code', $language_code)->first();
		
		//If the translation does not exist, return the default text
		if($translation)
			return $translation->value;

		return $this->value;
	}

	public function __toString()
	{
		return $this->value(\App::getLocale());
	}


}
