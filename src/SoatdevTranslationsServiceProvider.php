<?php

namespace Soatdev\Translations;

use Illuminate\Support\ServiceProvider;

class SoatdevTranslationsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'vendor/soatdev/translations/src/views', 'soatdevtranslations');

        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/soatdev/translations'),
        ]);

        $this->publishes([
            __DIR__ . '/database/migrations' => $this->app->databasePath() . '/migrations'
        ], 
        'migrations');

        \Form::macro('text_translatable', function ($name, $label, Text $text=null, $languages, $placeholder='', $type = 'textbox', $required = false) {

            $divsHtml = '';
            $lisHtml = '';
            $requiredHtml = '';

            if($required)
                $requiredHtml = '<span style="color:red;">*</span>';

            foreach($languages as $lang)
            {
                //Value
                $value = '';
                if(!is_null($text)){
                    $value = $text->value($lang->code);
                }

                //Icon
                $langIcon = '<i>'.strtoupper($lang->code).'</i>';
                if($lang->incon_url){
                    $langIcon ='<img src="'.$lang->incon_url.'"/>';
                }

                //List item
                $li = '
                            <li>
                                <a href="#'.$name.'-multilangtext-tab-'.$lang->code.'">
                                    '.$langIcon.'
                                </a>
                            </li>
                ';
                $lisHtml .= $li;

                //Div
                $form = '';
                if($type == 'textbox'){
                    $form = \Form::text(
                        $name.'['.strtolower($lang->code).']',                    
                        $value, 
                        array(
                            "style"=>"width:100%;",
                            'id'=>$name.ucfirst(strtolower($lang->code))
                        )
                    );
                }
                else
                {
                    $form = \Form::textArea(
                        $name.'['.strtolower($lang->code).']', 
                        $value,
                        array(
                            'class'=>'cleditor', 
                            "style"=>"width:100%;",
                            'placeholder'=>$placeholder, 
                            'id'=>$name.ucfirst(strtolower($lang->code))
                        )
                    );
                }
                $div = '
                        <div id="'.$name.'-multilangtext-tab-'.$lang->code.'" placeholder="'.$placeholder.'">
                            '.
                            $form
                            .'
                        </div>
                ';
                $divsHtml .= $div;


            }

            $html = '
                <style>
                    .form-tabs ul{
                        border-bottom-right-radius:0px;
                        border-bottom-left-radius:0px;
                    }
                    .ui-tabs .ui-tabs-nav .ui-tabs-anchor {
                        float: left;
                        padding: .1em .4em;
                        text-decoration: none;
                    }
                    .ui-tabs .ui-tabs-nav li {
                        list-style: none;
                        float: right;
                        position: relative;
                        top: 0;
                        margin: 1px .1em 0 0;
                        border-bottom-width: 0;
                        padding: 0;
                        white-space: nowrap;
                    }

                    .ui-tabs .ui-tabs-panel {
                        display: block;
                        border:1px solid rgb(174, 208, 234);
                        padding: .5em .4em;
                        background: none;
                    }
                </style>
                <div id="'.$name.'-edit-form">
                    <div id="'.$name.'-tabs" class="form-tabs"  style="border:none;display:inline-block;width:99%;">
                        
                        <ul>
                            <li style="float:left;">
                                <label for="'.$name.'" id="'.$name.'_label" class="field-title">'.$label.'</label>'.$requiredHtml.'
                            </li>
                            '.$lisHtml.'
                        </ul>
                        '.$divsHtml.'
                    </div>
                    '.
                    (isset($errors) && $errors->has($name) ? $errors->first($name) : '')
                    .'
                </div>
                <script>    
                    $( "#'.$name.'-tabs" ).tabs();
                </script>
            ';
            return $html;
        });
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
    }
}
