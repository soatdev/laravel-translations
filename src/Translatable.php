<?php
namespace SoatdevTranslations;

trait Translatable {

    //Translations Trait

    //This array defines the fields that have to be presented in the form of a textarea when creating or editing. By default, they all are presented in a textbox.
    public $textareas = array();

    public function texts()
    {
    	return Text::allForEntity($this);
    }
    public function getText($code)
    {
    	return Text::fromCodeForEntity($code, $this);
    	//fromCodeForEntity($code, $entity, $identifying_attribrute = 'id')
    }
    public function setText($code, $value, $language_code=null)
    {
    	return Text::add($this, $code, $value, $language_code);
    }

    public function getAttributeText($attribute, $language_code = null){
    	if(is_null($language_code)){
            if($this->language_code){
    		  $language_code = $this->language_code;
            }
            else{
                $language_code = \App::getLocale();
            }
    	}
        
    	if(method_exists ( $this , 'getText' )){
            if($this->getText($this->$attribute))
    	       return $this->getText($this->$attribute)->value($language_code);
    	}
    	return $this->$attribute;
    }

    public function setAttributeText($attribute, $value, $language_code = null){
    	if(is_null($language_code)){
    		$language_code = $this->language_code;
    	}
    	if(method_exists ( $this , 'setText' )){
    		$this->$attribute = $attribute;
    		$this->save();
    		return $this->setText($attribute, $value, $language_code);
    	}
    	else{
    		$this->$attribute = $value;
    		$this->save();
    	}
    }
}