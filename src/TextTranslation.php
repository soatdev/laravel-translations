<?php

namespace SoatdevTranslations;

use Illuminate\Database\Eloquent\Model;

class TextTranslation extends Model
{
    protected $fillable = ['text_id', 'language_code', 'value'];
	
}
