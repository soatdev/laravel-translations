<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextsTable extends Migration {

	public function up()
	{
		Schema::create('texts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('code', 50);
			$table->string('label');

			$table->string('owner_class');
			$table->string('owner_id_attribute');
			$table->string('owner_id_value');

			$table->string('language_code', 3)->default('en');
			$table->text('value');

			$table->timestamps();
		});

		Schema::table('texts', function(Blueprint $table) {
			$table->foreign('language_code')->references('code')->on('ctrl_languages')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('texts');
	}
}