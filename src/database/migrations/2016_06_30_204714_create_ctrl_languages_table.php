<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCtrlLanguagesTable extends Migration {

	public function up()
	{
		Schema::create('ctrl_languages', function(Blueprint $table) {
			$table->string('code', 3)->unique()->primary();
			$table->string('locale');
			$table->string('lcid_string');
			$table->string('icon_url');
			$table->boolean('is_active')->default(1);
			$table->timestamps();
		});

		
	}

	public function down()
	{
		Schema::drop('ctrl_languages');
	}
}