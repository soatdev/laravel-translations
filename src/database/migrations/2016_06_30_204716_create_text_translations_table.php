<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTextTranslationsTable extends Migration {

	public function up()
	{
		Schema::create('text_translations', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('text_id')->unsigned();
			$table->string('language_code', 3);

			$table->text('value');
			$table->timestamps();

			$table->unique(['text_id','language_code']);
		});

		Schema::table('text_translations', function(Blueprint $table) {
			$table->foreign('text_id')->references('id')->on('texts')
						->onDelete('cascade')
						->onUpdate('restrict');
		});

		Schema::table('text_translations', function(Blueprint $table) {
			$table->foreign('language_code')->references('code')->on('ctrl_languages')
						->onDelete('restrict')
						->onUpdate('no action');
		});
	}

	public function down()
	{
		Schema::drop('text_translations');
	}
}